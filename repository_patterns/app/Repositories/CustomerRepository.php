<?php


namespace App\Repositories;


use App\Customers;

class CustomerRepository
{
    public static function findByName($name)
    {
        return Customers::query()
            ->orderBy('name')
            ->where('name',$name)
            ->with('user')
            ->get();
    }

    public static function findById($id)
    {
        return Customers::query()
            ->orderBy('name')
            ->where('id',$id)
            ->with('user')
            ->get();
    }
}
