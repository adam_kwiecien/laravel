<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{

    public function format()
    {
        return[
            'customer_id' => $this->customer_id,
            'name' => $this->name,
            'contacted_at' => $this->contacted_at->diffForHumans(),
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
