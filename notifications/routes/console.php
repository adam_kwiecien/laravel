<?php

use App\Notifications\InvoicePaid;
use App\User;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');





Artisan::command('notifyUser{id}', function ($id) {
    $user = User::findOrFail($id);
    $user->notify(new InvoicePaid());
    $this->comment("User has been notified");
})->describe('Send an invoice test notification to a user with given id');

Artisan::command('ReadUserNotification{id}{notificationid}', function ($id, $notificationid) {
    $user = User::findOrFail($id);
    $user->notifications->find($notificationid)->markAsRead();
    $this->comment("User has read the notification");
})->describe('Reads user\'s notification');

Artisan::command('DisplayUserNotifications{id}', function ($id) {
    $user = User::findOrFail($id);
    dd($user->notifications->all());
//    dd($user->unreadNotifications->all());
//    dd($user->readNotifications->all());

})->describe('Reads user\'s notification');
