<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test',function ()
{
    Str::mymacro();
});

Route::get('/test2',function ()
{
    Str::MySecondMacro(123);
});

Route::get('/test3',function ()
{
    Str::MyThirdMacro(1200);
});
