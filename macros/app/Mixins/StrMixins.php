<?php


namespace App\Mixins;


class StrMixins
{
    public function MySecondMacro()
    {
        return function ($arg){
            dd($arg." zl");
        };
    }

    public function MyThirdMacro()
    {
        return function ($arg){
            dd($arg." PLN");
        };
    }
}
