<?php


namespace App\Http\View\Composer;


use Illuminate\View\View;

class TestComposer
{
    protected $os = 'windows';
    public function compose(View $view)
    {
        $view->with('os',$this->os);
    }
}
