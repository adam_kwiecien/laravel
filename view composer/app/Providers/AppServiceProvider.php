<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $username = 'Adam';
        $weather = 'sunny';

        //option 1
        View::share('name',$username);

        //option 2
        View::composer('posts.show',function ($view) use ($weather) {
            $view->with('weather',$weather);
        });

        //option 3
        View::composer('posts.show','App\Http\View\Composer\TestComposer');
    }
}
