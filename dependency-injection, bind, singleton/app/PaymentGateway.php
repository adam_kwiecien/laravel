<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    protected $fee;
    protected $currency;

    public function __construct($currency)
    {
        $this->currency=$currency;
        $this->fee=0; //in the smallest unit
    }

    public function pay($amount)
    {
        $this->fee += $amount;
        return [
            'amount' =>$this->fee,
            'currency' => $this->currency,
            'fee' =>$this->fee*0.2 ,
        ];
    }
}
