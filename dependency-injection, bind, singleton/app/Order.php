<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $order;
    protected $payment;

    public function __construct(PaymentGateway $payment)
    {
        $this->payment=$payment;
    }

    public function makeOrder()
    {
        $this->payment->pay(1230);
        return true;
    }
}
