<?php

namespace App\Providers;

use App\Payment\Pay;
use App\PaymentGateway;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    protected $currency = 'pln';

    public function register()
    {
//        $this->app->bind(PaymentGateway::class,function ($app){
//            return new PaymentGateway($this->currency);
//        });


        $this->app->singleton(PaymentGateway::class,function ($app){
            return new PaymentGateway($this->currency);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
