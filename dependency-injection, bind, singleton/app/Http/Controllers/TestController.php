<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentGateway;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TestController extends Controller
{
    public function index(PaymentGateway $payment,Order $order)
    {

        $order->makeOrder();
        dd($payment->pay(1200));
    }

    public function show(User $user)
    {
        dd($user);
    }

}
