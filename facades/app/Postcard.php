<?php


namespace App;

use 	Illuminate\Foundation\Application;

class Postcard
{

    public static function resolveFacade($method)
    {
        return app()->make($method);
    }

    public static function __callStatic($method, $arguments)
    {
        return (self::resolveFacade('Post'))->$method(...$arguments);
    }


}
