<?php

use App\Http\Controllers\PostcardSendingService;
use App\Postcard;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test',function ()
{
    Postcard::hello('someone');
});


Route::get('/test2',function ()
{
    $post = new PostcardSendingService('pl','l');
    $post->hello('someone');
});
