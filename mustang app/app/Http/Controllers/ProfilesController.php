<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware();
//    }

    public function index($user)
    {
        $user = User::findOrFail($user);


        $ownprofile=false;
        $follows = false;

        if(auth()->user())
        {
            $ownprofile=($user->id==Auth::user()->id)?true:false;
            $profiles = auth()->user()->following;
            $follows = false;

            foreach ($profiles as $followingUserId)
            {
                if($followingUserId["user_id"] === $user->id)
                {
                    $follows = true;
                }
            }
        }

        $postCount = Cache::remember('count.post.'.$user->id,200,function () use ($user)
        {
            return $user->posts->count();
        });

        $followersCount = Cache::remember('count.followers.'.$user->id,200,function () use ($user)
        {
            return $user->profile->followers->count();
        });

        $followingCount = Cache::remember('count.following.'.$user->id,200,function () use ($user)
        {
            return $user->following->count();
        });

        return view('posts/index',compact('user','follows' ,'ownprofile','postCount','followersCount','followingCount'));
    }

    public function edit()
    {
        $user = Auth::user();
        if($user==null ) return redirect('/login');

        $this->authorize('update',$user->profile);

        return view('profiles/edit',compact('user'));
    }

    public function update()
    {
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => '',
            'image' => 'image',
        ]);

        $user = Auth::user();

        if(request('image'))
        {
            $imagePath = request('image')->store('profile','public');

            $image = Image::make(public_path("storage/$imagePath"))->fit(1000,1000);
            $image->save();
            $data['image']=$imagePath;
        }

        $user->profile->update($data);

        return redirect("profile/".Auth::user()->id);
    }

    public function redirect()
    {
        $user = Auth::user();
        if($user==null ) return redirect('/login');


        $profiles = auth()->user()->following;
        $follows = false;

        foreach ($profiles as $followingUserId)
        {
            if($followingUserId["user_id"] === $user->id)
            {
                $follows = true;
            }
        }




        $postCount = Cache::remember('count.post.'.$user->id,200,function () use ($user)
        {
            return $user->posts->count();
        });

        $followersCount = Cache::remember('count.followers.'.$user->id,200,function () use ($user)
        {
            return $user->profile->followers->count();
        });

        $followingCount = Cache::remember('count.following.'.$user->id,200,function () use ($user)
        {
            return $user->following->count();
        });

        $ownprofile = true;
        return view('posts/index',compact('user','follows' ,'ownprofile','postCount','followersCount','followingCount'));

           }


}
