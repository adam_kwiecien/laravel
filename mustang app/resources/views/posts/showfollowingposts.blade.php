@extends('layouts.app')

@section('content')
    <style>
        /* unvisited link */
        a:link {
            color: black;
            text-decoration: none;
        }

        /* visited link */
        a:visited {
            color: black;
            text-decoration: none;
        }

        /* mouse over link */
        a:hover {
            color: black;
            text-decoration: none;
        }

        /* selected link */
        a:active {
            color: black;
            text-decoration: none;
        }
    </style>
    <div class="container">
             @foreach($posts as $post)

        <div class="row col-6 offset-2 border p-0 mb-4 pt-1 pb-1 bg-light shadow-sm">
            <a href="/profile/{{$post->user->id}}">
                <div class="d-flex m-lg-2">
                        <img class="rounded-circle mr-3" style="height: 50px"; src="{{URL::asset("storage/".$post->user->profile->image)}}" alt="">
                        <div>
                            <h3 class="m-0">{{$post->user->username}}</h3>
                            <h6 > {{$post->created_at->toDateString()}}</h6>
                        </div>
                </div>
            </a>

            <img src="{{URL::asset("storage/$post->image")}}" alt="" class="w-100">
            <div class="ml-2">{{$post->caption}}</div>

        </div>

                 @endforeach
        <div class="row">
            <div class="col-12 d-flex justify-center">
                {{$posts->links()}}
            </div>
        </div>
    </div>


@endsection
