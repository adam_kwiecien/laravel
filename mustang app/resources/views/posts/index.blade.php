@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row pl-5 pr-5">
        <div class="col-3 p-5">
            @if (Auth::check() && $ownprofile)
                <img  data-toggle="modal" data-target="#exampleModalLong" src="{{URL::asset('storage/'.$user->profile->image)}}" class="rounded-circle" style="height: 150px;">

                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content text-center">
                            <div class="modal-header ">
                                <h5 class="modal-title" id="exampleModalLongTitle">Options</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body ">
                                <div><a href=/p/create>Add new photo to gallery</a></div>
                                <hr>
                                <div><a href=/profile/edit>Edit Profile</a></div>
                            </div>

                        </div>
                    </div>
                </div>

            @else
                <img src="{{URL::asset('img/userimage.jpg')}}" class="rounded-circle" style="height: 150px;">
            @endif

        </div>
        <div class="col-5 pt-5">
            <div class="pb-2 d-flex"><h2>{{ $user->name }}</h2>  @if (Auth::check() && !$ownprofile) <follow-button user-id="{{$user->id}}" follows="{{$follows}}" ></follow-button> @endif</div>



            <div class="d-flex">
                <div class="pr-5"><strong>{{$postCount}}</strong> posts</div>
                <div class="pr-5"><strong>{{$followersCount}}</strong> followers</div>
                <div class="pr-5"><strong>{{$followingCount}}</strong> following</div>
            </div>
            <div class=" pt-4 font-weight-bold"><a>{{ $user->profile->title }}</a></div>
            <div >{{ $user->profile->description }}</div>
            <div >{{ $user->profile->url }}</div>

        </div>
            <div class="row pt-5">
                @foreach($user->posts as $post)

                        <div class="col-4 pb-4" >
                            <a href="/p/{{$post->id}}">
                                <img  class="w-100" src="{{URL::asset('storage/'.$post->image)}}">
                            </a>
                        </div>

                @endforeach



            </div>


    </div>
</div>
@endsection
