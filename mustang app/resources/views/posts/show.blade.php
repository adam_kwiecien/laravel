@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row col-10">
            <div class="col-7">
                <img src="{{URL::asset("storage/$post->image")}}" alt="" class="w-100">
            </div>
            <div class="col-5">
                <div>

                    <div class="d-flex align-items-center mb-4">

                        <img class="rounded-circle mr-3" style="height: 50px"; src="{{URL::asset("storage/".$post->user->profile->image)}}" alt="">
                        <div class="mr-2">
                            <h3 class="m-0">{{$post->user->username}}</h3>

                            <h6 > {{$post->created_at->toDateString()}}</h6>
                        </div>
                        @if (Auth::user()->id!=$post->user->id)
                            <a class="btn btn-primary w-100 h4" href="#">Follow</a>
                        @endif
                    </div>
<hr>
                    <p class="pl-1">
                        {{$post->caption}}
                    </p>

                </div>
            </div>
        </div>
    </div>


@endsection
