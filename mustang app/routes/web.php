<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/email',function ()
{
    return new \App\Mail\NewUserWelcome();
});

Route::get('/', 'PostsController@index');
Route::get('/profile/edit', 'ProfilesController@edit');
Route::patch('/profile', 'ProfilesController@update');
Route::get('/profile', 'ProfilesController@redirect');
Route::get('/profile/{user}', 'ProfilesController@index')->name('profile.show');

Route::post('follow/{user}', 'FollowsController@store');

Route::get('/p/create', 'PostsController@create');
Route::post('/p', 'PostsController@store');
Route::get('/p/{post}', 'PostsController@show');

