<form name="test" method="post">
    @csrf
    @method('put')
    <input name="name" @error('name')style="color: red;" @enderror>
    @error('name'){{$message}} @enderror
    <input type="submit" value="Zapisz">
</form>
