<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

//dummy post
Artisan::command('dp', function () {
    $post = new \App\Post();
    $post->title = 'test';
    $post->content = 'test test test test test test test test test test test test test test';
    $post->save();
    $post->image()->create(['url'=>'test.jpg']);
})->describe('Creates dummy post');


//dummy user
Artisan::command('du', function () {
    $user = new \App\User();
    $user->name = 'Test';
    $user->email = 'test@gmail.com';
    $user->password = '$2y$10$JpgdH.z3A.n5w.louQDvReokR1SKqgc528mia5Bw24IGG3YKr5bfm'; //123123123
    $user->save();
    $user->image()->create(['url'=>'test.jpg']);
})->describe('Creates dummy user');

//dummy somethingelse
Artisan::command('dse', function () {
    $se = new \App\SomethingElse();
    $se->name = 'test';
    $se->save();
    $se->image()->create(['url'=>'test.jpg']);
    $se->image()->create(['url'=>'test2.jpg']);
    $se->image()->create(['url'=>'test3.jpg']);
})->describe('Creates dummy user');
