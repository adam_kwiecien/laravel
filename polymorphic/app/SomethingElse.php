<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SomethingElse extends Model
{
    protected $guarded = [];
    public function image()
    {
        return $this->morphMany(Images::class,'imagesable');
    }
}
