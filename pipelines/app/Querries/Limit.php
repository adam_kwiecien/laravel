<?php


namespace App\Querries;


class Limit extends AbstractFilter {
    protected $has = "max";
    protected function applayFilter($builder)
    {
        return $builder->limit(request()->get('max'));
    }
}
