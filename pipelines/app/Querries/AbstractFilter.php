<?php


namespace App\Querries;


abstract class AbstractFilter {
    protected $has = "";
    public function handle($request, \Closure $next)
    {
        if (!request()->has($this->has)){
            return $next($request);
        }

        $builder = $next($request);

        return $this->applayFilter($builder);
    }

    abstract protected function applayFilter($builder);

}
