<?php


namespace App\Querries;


class Order extends AbstractFilter {
    protected $has = "order";

    protected function applayFilter($builder)
    {
        return $builder->orderBy('title',request()->get('order'));
    }
}
