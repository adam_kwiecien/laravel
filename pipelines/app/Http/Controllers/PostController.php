<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class PostController extends Controller
{
    function show()
    {
        $posts = Post::query();


        //METODA Z UŻYCIEM PIPELINES
        $posts = app(Pipeline::class)
            ->send($posts)
            ->through([
                \App\Querries\Active::class,
                \App\Querries\Order::class,
                \App\Querries\Limit::class,

            ])
            ->thenReturn()->paginate(request()->has('max')?request('max'):5);



        //ZWYKŁA METODA
//        if( request()->get('active')!=null)
//        {
//            $posts = $posts->where("active","=", request()->get('active'));
//        }
//
//        if( request()->get('order')!=null && in_array(request()->get('order'),["asc","desc"]))
//        {
//            $posts = $posts->orderBy('title',request()->get('order'));
//        }
//
//        if( request()->get('max')!=null && is_numeric(request()->get('max')))
//        {
//            $posts = $posts->limit(request()->get('max'));
//        }
//
//        $posts = $posts->get('*');

        return view('post',compact('posts'));
    }
}
